[返回首页](home)
---
# git 使用规范

### 修订历史记录 

| 日期 | 维护者 | 版本 | 说明 |
| -------- | -------- | -------- | -------- |
| 20160621 | ouyang | 1.0 | 初稿 |

### 【 主要概念】

#### 1. 工作目录/暂存区/仓库
     对于任何一个文件，在 Git 内都只有三种状态：已提交（committed），已修改（modified）和已暂存（staged）。已提交表示该文件已经被安全地保存在本地数据库中了；已修改表示修改了某个文件，但还没有提交保存；已暂存表示把已修改的文件放在下次提交时要保存的清单中。
     由此我们看到 Git 管理项目时，文件流转的三个工作区域：Git 的工作目录，暂存区域，以及本地仓库。
     基本的 Git 工作流程如下：
* 在工作目录中修改某些文件。
* 将修改后的文件保存到暂存区域。
* 提交更新，将保存在暂存区域的文件快照永久转储到 Git 仓库中。
* git仓库不仅存储了当前的提交(commit)，还存储了所有的提交历史，通过修改HEAD指针可以在任何提交历史间进行切换。

#### 2. 提交（commit）
     在git中每次提交将产生一个commit，commit记录了提交的所有文件内容、提交时间、提交人以及它的父commit。根据父commit信息可以绘制出一张链状图（见SourceTree）。一个commit可能有多个父commit，比如分支合并时产生的commit。git根据commit的所有信息计算出一个哈希值（40个十六进制数）作为commit的ID，该ID是全球唯一的。

#### 3. 引用（reference）
     由于commit没有名称只有一个ID，非常难以记忆，所以git系统使用引用来指向一个commit，并使用引用名称来代替commit ID。分支（branch）、远程分支、标签（tag）都属于引用。在git系统中，只有被引用指向的commit以及它的祖先commit是可见的（在sourceTree中显示），其它commit被视为垃圾，保留数月之后会被清理。所以，删除commit只需要移动或删除相关引用即可，git没有提供直接删除commit的方法。

#### 4. 分支（branch）
     分支是引用的一种，与其它引用不同的是，分支可以自由移动指向任意commit，而其它引用创建后不可移动。另外HEAD指针可以指向一个分支，称为当前分支，提交时当前分支会自动指向新的commit。
     远程分支是远程仓库响应分支在本地的代表。远程分支虽然也叫分支，但是不能自由移动，它只在与服务器通信时，同步服务器相应分支的状态。远程分支命名为<远程仓库名/分支名>。

#### 5. HEAD指针
     HEAD指针可视为一种特殊的引用，它可以指向一个branch，也可以直接指向一个commit。直接指向commit的HEAD指针称为分离HEAD指针，此时如果提交，就只有HEAD指针指向新的commit，再将HEAD指向其它分支时，新commit将变为不可见，所以提交时应确保HEAD指向一个分支。HEAD所指向（直接或间接）的commit为当前使用的commit。

#### 6. 标签（tag）
     为了快速找到某些阶段性版本，可以对这些版本打上tag。tag也是一种引用，它创建后不可移动。tag有两种类型，一种是轻量级tag，它就是一个引用，保存了一个commit ID，另一种是带注释的tag，它创建了一个tag对象，保存了tag创建人、创建时间以及tag指向的commit ID等信息，并且能够添加一些注释信息，再用一个引用指向该tag对象。通常推荐使用带注释的tag。

#### 7. 远程仓库抓取与推送
     git大部分指令都在本地完成，无需联网，只有fetch和push指令需要访问服务器。fetch指令将远程新的commit数据打包下载，然后将本地的远程分支更新到远程仓库对应分支的状态。push指令将本地需要推送的commit数据打包上传，然后将远程仓库的指定分支更新到推送分支的状态。注意：推送时远程仓库分支原来指向的commit必须为新指向commit的祖先commit（这种情况称为快进式推送（fast forward）），否则，服务器将拒绝推送，此时必须在本地进行合并后再进行推送。

### 【 执行步骤】

#### 1. 初次使用
     使用以下命令设置自己的用户名和邮箱地址，用户名和邮箱地址会记录到你的每个提交中。
          git config --global user.name <your name>
          git config --global user.email <your email>
     SourceTree可在Preference->General中设置。

#### 2. 新建/克隆项目
     获取git仓库有两种方法：
     一是建立新的git仓库，创建方法是进入需要git管理的目录，执行：
          git init
     二是从现有仓库克隆，执行：
          git clone <url>
     或使用SourceTree：

     如果克隆的仓库包含子模块（submodule），就需要执行以下命令将子模块也克隆到本地：
          git submodule update --init

#### 3. 提交修改
     在工作目录添加、修改或删除文件后，使用以下命令查看当前仓库状态：
          git status
     命令执行后会显示当前未暂存的修改和已暂存的修改。
     然后将准备提交的文件加入暂存区：
          git add <file path>
     最后将暂存区内容提交到git仓库中：
          git commit
     命令执行后需要填写提交信息，完成后才生成一个新的提交。
     如果发现上次提交有问题，可以使用以下命令修改上次提交（使用时务必确保当前提交没有push到服务器）：
          git commit --amend

     使用SourceTree：

     其中显示了Staged files和Unstaged files，点击或拖动可以切换staged状态。
     填写提交信息后点击Commit就提交了，可以选择Commit options中的Amend last commit来修改上次提交。

#### 4. 与服务器同步
     在本地进行了一系列提交之后，准备更新到远程服务器，更新前先要拉取远程的最新更新，使用：
          git pull -r
     该命令会拉取远程更新并与你的修改进行合并，合并中可能产生冲突，解决冲突后将冲突文件add到暂存区再使用：
          git rebase --continue
     pull完成之后就可以将本地修改更新到服务器了，使用：
          git push origin develop
     SourceTree点击Pull后界面，勾选上以下选项：

     SourceTree点击Push后界面，勾选需要push的分支，如果是新建分支勾选Track：

#### 5. 分支开发
     开发时可能需要使用分支开发，具体何时使用分支及如何使用请参考【Guideline-DEV-02. git 工作流程】。
     首先新建分支：
          git branch <new_branch_name>
     然后切换到新分支（SourceTree中双击对应分支即可切换）：
          git checkout <new_branch_name>
     在该分支上工作后，如果需要更新到服务器，执行命令：
          git push -u origin <new_branch_name>
     以上命令在新分支第一次push时使用，以后push与步骤4相同。
     该分支功能开发测试完毕后，需要将新分支的修改合并到原来的分支，先切换回原分支，拉取服务器的更新，然后使用合并命令：
          git merge <new_branch_name>
     合并中可能产生冲突，解决冲突后将冲突文件add到暂存区再使用：
          git commit
     最后将原分支push，新分支合并到原分支后可删除。
     在SourceTree中新建分支，点击工具栏Branch按钮，填写名称，确定即可。

     在SourceTree中合并分支，点击工具栏Merge按钮，选择需要合并的分支，选项勾选如下图所示，确定即可。

#### 6. 子模块
     许多项目都需要用到公司共享的库，将库添加到项目中使用命令：
          git submodule add <lib_url> <path>
     或选择 SourceTree 的 Repository->Add Submodule菜单：

     如果需要更新库的版本，进入子模块目录，checkout到需要的版本，再回到原目录，此时查看仓库状态会显示有子模块被修改了，然后将其add到暂存区，并提交即可。
     在SourceTree中双击下图中submodule即可进入该submodule的仓库：

#### 7. 打标签
     给某个阶段性版本打上标签，使用命令：
          git tag -a <tag_name> -m <message>
     参数-a表示创建的是一个带注释的标签。
     将所有标签push到服务器：
          git push --tags origin
     在SourceTree中，点击工具栏Tag按钮，选择需要打标签的commit，填写标签名称，确定即可。


####【参考文献】

[Pro git中文版](http://git.oschina.net/progit/index.html)
[廖雪峰git教程](http://www.liaoxuefeng.com/wiki/0013739516305929606dd18361248578c67b8067c8c017b000)

## git 公司规范 
###【 核心原则】

#### 1.  Git 仓库命名规则
     APP仓库：App_ProjectName_iOS/App_ProjectName_Android
     库开发仓库：Lib_ProjectName_Server/Lib_ProjectName_iOS/Lib_ProjectName_Android
     库发布二进制文件仓库：Bin_ProjectName_Server/Bin_ProjectName_iOS/Bin_ProjectName_Android
     以上前两种属于开发仓库，后一种属于发布仓库，两种仓库工作流程不同，下面将分别讨论。

#### 2. 开发仓库工作流程
* 主分支 master

          git 仓库有且仅有一个主分支，主分支上的版本都是提供给用户使用的正式版本，禁止在master分支直接提交代码。
* 开发分支 develop

          主分支只用来发布正式版本，日常开发都应该在develop分支进行，开发测试稳定后再合并到master分支，并打上版本标签。
          develop分支合并到master分支一般由PM完成，先切换到master分支，再使用命令：
               git merge --no-ff develop
          参数 --no-ff 意思是即使是快进式合并，也产生一个新的commit。使用SourceTree merge时勾选Create a commit even if merge resolved via fast-forward。

* 功能分支 feature-xxx

          小功能的开发或小bug的修复一般可以直接在develop分支完成，而一些大功能的开发则需要新建功能分支进行开发，完成后合并到develop分支。
          需要建功能分支主要有以下几种情况：
               该功能需要较长时间完成（1天以上）
               该功能是一个测试性功能
               该功能不在接下来的版本中发布

* 预发布分支 release-xxx

          在所有功能基本开发完毕时，准备合并到master分支前，可以建立一个预发布分支，该分支只进行小bug修改，界面微调等操作，避免引入新的bug。
          开发完毕后将其合并到develop和master分支。
* 紧急修补分支 hotfix-xxx

          项目正式发布后，可能会出现严重的bug，这种情况就需要使用紧急修补分支。
          在master上新建紧急修补分支，进行bug修复，此时应选择最简单的修复方法，避免引入新的bug。完成后将其合并到develop和master分支。

#### 3. 发布仓库工作流程
* 主分支 master

          发布仓库中的master分支只用来保存GM版本和release版本。
* 测试版分支 develop

          库的日常版本更新都在develop分支完成，并打上beta标签。
          经过测试稳定后由专人将其合并到master分支，合并方法与开发仓库不同，直接将develop上的内容拷贝到master分支并提交，这样可以让develop分支和master分支没有交叉。
          使用命令合并的方法：
               git checkout master
               git reset --hard <develop上的稳定版本>
               git reset --soft master@{1}
               git commit
          这样做的目的是在需要时删除一些测试版历史以减小仓库大小。
          最后，禁止使用测试版上线。

#### 参考文献

[git flow](http://www.ituring.com.cn/article/56870)
