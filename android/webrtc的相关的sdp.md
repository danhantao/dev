# 参考链接
https://webrtchacks.com/sdp-anatomy/  
http://www.rtc8.com/archiver/?tid-29.html=

# 上网的过程
```
内网，上网的过程中，我们没有外网ip。
有两种做法:
1.路由器开一个外网端口。
比如A访问百度，那么经过固定的端口0001，然后到达固定服务器。
然后数据回到某个端口0001,数据转发给A。
tcp面向连接。
udp不是面向连接的，我们不知道它什么时候会断掉。

2.路由器不是开一个固定的端口，而是通过映射，将A访问的数据为百度请求，如果回来是百度请求，
依然回到A数据。

如果是第一种情况，是可以打洞的。

NAT(Network Address Translation) 网络地址转换

NAT的实现方式有三种，
静态转换:一一对应
动态转换:一对多
端口多路复用(Port address Translation):端口复用。
ALG(Application leve gateway):通过协议，进行NAT转换。

固定的端口，可以打点，因为知道端口是有用的。
不固定的端口，不知道什么时候失效，这个时候打洞，很难穿透。

p2p打洞，时间过长，这个时候的解决方案是:
先通过tuns服务器relay视频，等连接十分钟后，切换为p2p。
这里面涉及到ice restart方法。

通过js代码，看到ice restart的时候，视频会闪一下，而不是黑一下以后，才能再次连接。
```

# 修改ICE Candidate
```
可以通过修改协议，比如不同的机型，修改sdp的报文信息。
比如打洞，删除掉 typ srflx 这样就不会打洞了。
如果是转发:ICE Candidates中只有relay关系。
WebRtc
configure.iceRestart = true;
configure.iceTransportPolicy = "all";

黑科技，修改一些协议的报文，伪装数据，达到针对性的来节省流量。
节省电量，节省数据的办法。
比如webview的缓存机制，可以导致省电。

```

# 解析SDP报文
```
分析报文内容，可以看到视频的audio和video，audio耗的流量过小，而video耗的流量大。
如果有多个turn服务器，可能会导致双倍的流量,relay转发会导致流量过多。

黑屏问题，其实很多时候，是编解码问题，coder不一样。
```