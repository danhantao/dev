Android性能优化
===============
### 1.性能分析工具     [参考](http://www.jianshu.com/p/da2a4bfcba68)
![Image](pictures/Performance_analysis_tool.png)  
####1.官方工具
######1.1 StrictMode
[参考](http://developer.android.com/reference/android/os/StrictMode.html)
[参考](http://www.jianshu.com/p/2ebc9363ea16)
[参考](http://blog.csdn.net/meegomeego/article/details/45746721)
```
严格模式主要检测两大问题:
一个是线程策略，即TreadPolicy
StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                .detectAll()//开启所有的detectXX系列方法
                .penaltyDialog()//弹出违规提示框
                .penaltyLog()//在Logcat中打印违规日志
                .build());
另一个是VM策略，即VmPolicy
 StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                .detectActivityLeaks()//检测Activity泄露
                .penaltyLog()//在Logcat中打印违规日志
                .build());
```
######1.2 Systrace
[参考](https://developer.android.com/studio/profile/systrace.html)  

######1.3 Hierarchy Viewer 
[参考](https://developer.android.com/studio/profile/hierarchy-viewer.html)
[参考](https://developer.android.com/studio/profile/hierarchy-viewer-walkthru.html)  

######1.4 TraceView
[参考](https://developer.android.com/studio/profile/traceview.html)
[参考](https://developer.android.com/studio/profile/traceview-walkthru.html)  

######1.5 Memory Monitor
[参考](http://www.jianshu.com/p/ef9081050f5c)  

######1.6 MAT  
[参考](http://www.jianshu.com/p/2d47d1cf5ccf)  

######1.7 Other Monitor
[CPU](https://developer.android.com/studio/profile/am-cpu.html)
[GPU](https://developer.android.com/studio/profile/am-gpu.html)
[NetWork](https://developer.android.com/studio/profile/am-network.html)  

######1.8 其他

####2.第三方工具
######2.1 [Google的Battery Historian](https://github.com/google/battery-historian)
######2.2 [网易的Emmagee](https://github.com/NetEase/Emmagee)
######2.3 [Square的Leakcanary](https://github.com/square/leakcanary)
######2.4 [AndroidDevMetrics](https://github.com/frogermcs/AndroidDevMetrics)

### 2.启动速度
[参考1](http://www.jianshu.com/p/98c1656a357a)  
[参考2](http://www.jianshu.com/p/4f10c9a10ac9)  
[参考3](https://developer.android.com/topic/performance/launch-time.html)  


### 3.布局优化
[参考](http://www.jianshu.com/p/4943dae4c333)  

### 4.ANR问题
[参考](http://www.jianshu.com/p/6d855e984b99)  

### 5.卡顿问题
[参考](http://www.jianshu.com/p/1fb065c806e6)  

### 6.内存优化
[参考1](http://www.jianshu.com/p/48475df838d9)  
[参考2](http://www.jianshu.com/p/c0e5c13d5ecb)

<<<
全局内存查看
adb shell dumpsys meminfo [package-name]
<<<

* 1、java的内存区域如何划分？
```
一种说法是分为: 
堆（Heap)
栈（Stacks）
方法区（MethodArea）
运行时常量池（RuntimeConstant Pool）
本地方法栈（NativeMethod Stacks）
PC Register(PC寄存器)。
是从抽象的JVM的角度去看的。

另一种说法是分为:
堆（Heap）
栈（Stacks）
数据段（data segment）
代码段（code segment）
是从操作系统上的进程的角度去看的。
```
按照第一种说法划分:  
![内存划分图](pictures/java_memory.png)

Heap/Stack
**在这问题中，我们主要要弄清楚，什么是堆，什么是栈，堆栈内存有什么区别?**
```
Heap内存的分配也叫做动态内存分配，java中运行环境用来分配给对象和JRE类的内存都在堆内存，C/C++有时候可以用malloc或者new来申请分配一个内存。在C/C++可能需要自己负责释放（java里面直接依赖GC机制）。
Stack内存是相对于线程Thread而言的, 在执行函数(方法)时，函数一些内部变量的存储都可以放在栈上面创建，函数执行结束的时候这些存储单元就会自动被释放掉。栈内存包括分配的运算速度很快，因为内置在处理器的里面的。当然容量有限。它保存线程中方法中短期存在的变量值和对Heap中对象的引用等.
区别：堆是不连续的内存区域，堆空间比较灵活也特别大。 栈式一块连续的内存区域，大小是有操作系统觉决定的。堆管理很麻烦，频繁地new/remove会造成大量的内存碎片，这样就会慢慢导致效率低下。对于栈的话，他先进后出，进出完全不会产生碎片，运行效率高且稳定。

我们通常说的内存泄露，GC，是针对Heap内存的. 因为Stack内存在函数出栈的时候就销毁了。
比如说这个类

public class People{
    int a = 1;
    Student s1 = new Student();
    public void XXX(){
        int b = 1;
        Student s2 = new Student();
    }
}
请问a的内存在哪里，b的内存在哪里，s1，s2的内存在哪里？记住下面两句话。

成员变量全部存储在堆中(包括基本数据类型，引用及引用的对象实体)，因为他们属于类，类对象最终还是要被new出来的。
局部变量的基本数据类型和引用存储于栈当中，引用的对象实体存储在堆中。因为他们属于方法当中的变量，生命周期会随着方法一起结束。
所以答案就是a，s1,s2对象都堆中，b和s2对象引用在栈中。
```

* 2、java中的引用有哪些？如何运用？
```
这四种级别由高到低依次为：强引用、软引用、弱引用和虚引用。

强引用(StrongReference) 我们使用的大部分引用实际上都是强引用，这是使用最普遍的引用。如果一个对象具有强引用，那就类似于必不可少的生活用品，垃圾回收器绝不会回收它。当内存空间不足，Java虚拟机宁愿抛出OutOfMemoryError错误，使程序异常终止，也不会靠随意回收具有强引用的对象来解决内存不足问题。
软引用(SoftReference) 如果内存空间足够，垃圾回收器就不会回收它，如果内存空间不足了，就会回收这些对象的内存。只要垃圾回收器没有回收它，该对象就可以被程序使用。
弱引用（WeakReference） 在垃圾回收器线程扫描它 所管辖的内存区域的过程中，一旦发现了只具有弱引用的对象，不管当前内存空间足够与否，都会回收它的内存。不过，由于垃圾回收器是一个优先级很低的线程， 因此不一定会很快发现那些只具有弱引用的对象。 弱引用可以和一个引用队列(ReferenceQueue)联合使用，如果弱引用所引用的对象被垃圾回收，Java虚拟机就会把这个弱引用加入到与之关联的引用队列中。
虚引用(PhantomReference) 如果一个对象仅持有虚引用，那么它就和没有任何引用一样，在任何时候都可能被垃圾回收。虚 引用主要用来跟踪对象被垃圾回收的活动。虚引用与软引用和弱引用的一个区别在于：虚引用必须和引用队列(ReferenceQueue)联合使用。当垃 圾回收器准备回收一个对象时，如果发现它还有虚引用，就会在回收对象的内存之前，把这个虚引用加入到与之关联的引用队列中。程序可以通过判断引用队列中是 否已经加入了虚引用，来了解被引用的对象是否将要被垃圾回收。程序如果发现某个虚引用已经被加入到引用队列，那么就可以在所引用的对象的内存被回收之前采取必要的行动。
```

* 3、什么是内存泄露？内存泄露发生的场景有哪些？
```
当一个对象已经不需要再使用了，本该被回收时，而有另外一个正在使用的对象持有它的引用，从而就导致对象不能被回收。这种导致了本该被回收的对象不能被回收而停留在堆内存中，就产生了内存泄漏。内存泄露问题，在下篇博客中会详细介绍把内存泄露抓出来。
内存泄露的场景有很多。

非静态内部类的静态实例
由于内部类默认持有外部类的引用，而静态实例属于类。所以，当外部类被销毁时，内部类仍然持有外部类的引用，致使外部类无法被GC回收。因此造成内存泄露。
类的静态变量持有大数据对象
静态变量长期维持到大数据对象的引用，阻止垃圾回收。

资源对象未关闭
资源性对象如Cursor、Stream、Socket，Bitmap，应该在使用后及时关闭。未在finally中关闭，会导致异常情况下资源对象未被释放的隐患。

注册对象未反注册
我们常常写很多的Listener,未反注册会导致观察者列表里维持着对象的引用，阻止垃圾回收。

Handler临时性内存泄露
Handler通过发送Message与主线程交互，Message发出之后是存储在MessageQueue中的，有些Message也不是马上就被处理的。
-Context泄露
这个太多了，不细说，单利模式写的不恰当就属于这种。
```
* 4、Garbage Collector(垃圾回收器)什么是垃圾，什么是非垃圾？
```
什么是GC？
GC 是 garbage collection 的缩写, 垃圾回收的意思. 也可以是 Garbage Collector, 也就是垃圾回收器.
垃圾回收机制有好几套算法，java语言规范没有明确的说明JVM 使用哪种垃圾回收算法，但是任何一种垃圾回收算法一般要做两件基本事情：（1）发现无用的信息对象；（2）回收将无用对象占用的内存空间。使该空间可被程序再次使用。
```

### 7.网络优化
[参考](http://www.jianshu.com/p/d4c2c62ffc35)  

### 8.电量优化
[参考](http://www.jianshu.com/p/c55ef05c0047)  

### 9.性能优化
[参考](http://www.jianshu.com/c/9a37aa095141)
