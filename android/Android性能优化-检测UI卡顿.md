Android性能优化-检测UI卡顿
======
### 1.Android UI性能优化实战 识别绘制中的性能问题 
[参考](http://blog.csdn.net/lmj623565791/article/details/45556391/)
```
Hierarchy Viewer
https://github.com/romainguy/ViewServer
```

### 2.如何检测应用在UI线程的卡顿
[参考1 以下方式1](https://github.com/markzhai/AndroidPerformanceMonitor)
[参考2 以下方式2](https://github.com/wasabeef/Takt)
[参考3 以下方式2](https://github.com/friendlyrobotnyc/TinyDancer)
##### 1.利用UI线程Looper打印的日志
```
new Handler(Looper.getMainLooper())
    .post(new Runnable() {
        @Override
        public void run()
        {}
    }
该代码在UI线程中的MessageQueue中插入一个Message，最终会在loop()方法中取出并执行。

假设，我在run方法中，拿到MessageQueue，自己执行原本的Looper.loop()方法逻辑，那么后续的UI线程的Message就会将直接让我们处理


Looper.java
    /**
     * Run the message queue in this thread. Be sure to call
     * {@link #quit()} to end the loop.
     */
    public static void loop() {
        final Looper me = myLooper();
        if (me == null) {
            throw new RuntimeException("No Looper; Looper.prepare() wasn't called on this thread.");
        }
        final MessageQueue queue = me.mQueue;

        // Make sure the identity of this thread is that of the local process,
        // and keep track of what that identity token actually is.
        Binder.clearCallingIdentity();
        final long ident = Binder.clearCallingIdentity();

        for (;;) {
            Message msg = queue.next(); // might block
            if (msg == null) {
                // No message indicates that the message queue is quitting.
                return;
            }

            // This must be in a local variable, in case a UI event sets the logger
            final Printer logging = me.mLogging;
            if (logging != null) {
                logging.println(">>>>> Dispatching to " + msg.target + " " +
                        msg.callback + ": " + msg.what);
            }

            final long traceTag = me.mTraceTag;
            if (traceTag != 0) {
                Trace.traceBegin(traceTag, msg.target.getTraceName(msg));
            }
            // 此处可以检测延时
            try {
                msg.target.dispatchMessage(msg);
            } finally {
                if (traceTag != 0) {
                    Trace.traceEnd(traceTag);
                }
            }

            if (logging != null) {
                logging.println("<<<<< Finished to " + msg.target + " " + msg.callback);
            }

            // Make sure that during the course of dispatching the
            // identity of the thread wasn't corrupted.
            final long newIdent = Binder.clearCallingIdentity();
            if (ident != newIdent) {
                Log.wtf(TAG, "Thread identity changed from 0x"
                        + Long.toHexString(ident) + " to 0x"
                        + Long.toHexString(newIdent) + " while dispatching to "
                        + msg.target.getClass().getName() + " "
                        + msg.callback + " what=" + msg.what);
            }

            msg.recycleUnchecked();
        }
    }



```
##### 2.利用Choreographer
```
   Choreographer.getInstance().postFrameCallback(new Choreographer.FrameCallback() {
            @Override
            public void doFrame(long frameTimeNanos) {
                // 处理检测逻辑
                Choreographer.getInstance().postFrameCallback(this);
            }
        });
```