自定义View
========
### 1.[Canvas之绘制基础形状](https://github.com/GcsSloop/AndroidNote/blob/master/CustomView/Advance/%5B02%5DCanvas_BasicGraphics.md)
```
为什么会有Rect和RectF两种？两者有什么区别吗？
两者最大的区别就是精度不同，Rect是int(整形)的，而RectF是float(单精度浮点型)。

绘制矩形
        // 第一种
        canvas.drawRect(100,100,800,400,mPaint);

        // 第二种
        Rect rect = new Rect(100,100,800,400);
        canvas.drawRect(rect,mPaint);

        // 第三种
        RectF rectF = new RectF(100,100,800,400);
        canvas.drawRect(rectF,mPaint);

 绘制圆角矩形
        // 第一种
        RectF rectF = new RectF(100,100,800,400);
        canvas.drawRoundRect(rectF,30,30,mPaint);

        // 第二种
        canvas.drawRoundRect(100,100,800,400,30,30,mPaint);

 绘制椭圆
       // 第一种
        RectF rectF = new RectF(100,100,800,400);
        canvas.drawOval(rectF,mPaint);

        // 第二种
        canvas.drawOval(100,100,800,400,mPaint);

 Paint
 如果你注意到了的话，在一开始我们设置画笔样式的时候是这样的：
 mPaint.setStyle(Paint.Style.FILL);  //设置画笔模式为填充
为了展示方便，容易看出效果，之前使用的模式一直为填充模式，实际上画笔有三种模式，如下：
STROKE                //描边
FILL                  //填充
FILL_AND_STROKE       //描边加填充
```