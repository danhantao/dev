View的绘制流程
========
[参考](http://a.codekk.com/detail/Android/lightSky/%E5%85%AC%E5%85%B1%E6%8A%80%E6%9C%AF%E7%82%B9%E4%B9%8B%20View%20%E7%BB%98%E5%88%B6%E6%B5%81%E7%A8%8B)
绘制流程图   
![Image](pictures/view_draw_method_chain.png)

```
整个View树的绘图流程是在ViewRoot.java类的performTraversals()函数展开的，该函数做的执行过程可简单概况为
 根据之前设置的状态，判断是否需要重新计算视图大小(measure)、是否重新需要安置视图的位置(layout)、以及是否需要重绘
 (draw)

invalidate()方法 ：
 
   说明：请求重绘View树，即draw()过程，假如视图发生大小没有变化就不会调用layout()过程，并且只绘制那些“需要重绘的”
视图，即谁(View的话，只绘制该View ；ViewGroup，则绘制整个ViewGroup)请求invalidate()方法，就绘制该视图。
 
     一般引起invalidate()操作的函数如下：
            1、直接调用invalidate()方法，请求重新draw()，但只会绘制调用者本身。
            2、setSelection()方法 ：请求重新draw()，但只会绘制调用者本身。
            3、setVisibility()方法 ： 当View可视状态在INVISIBLE转换VISIBLE时，会间接调用invalidate()方法，继而绘制该View。
            4 、setEnabled()方法 ： 请求重新draw()，但不会重新绘制任何视图包括该调用者本身。
 
    requestLayout()方法 ：会导致调用measure()过程 和 layout()过程 。
 
           说明：只是对View树重新布局layout过程包括measure()和layout()过程，不会调用draw()过程，但不会重新绘制
任何视图包括该调用者本身。
 
    一般引起invalidate()操作的函数如下：
         1、setVisibility()方法：
             当View的可视状态在INVISIBLE/ VISIBLE 转换为GONE状态时，会间接调用requestLayout() 和invalidate方法。
    同时，由于整个个View树大小发生了变化，会请求measure()过程以及draw()过程，同样地，只绘制需要“重新绘制”的视图。
 
    requestFocus()函数说明：
 
          说明：请求View树的draw()过程，但只绘制“需要重绘”的视图。
 

```
